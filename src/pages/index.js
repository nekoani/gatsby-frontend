/* eslint-disable no-unused-vars */
import React from 'react'
import logo from '../../static/logo.svg'
import styled from '@emotion/styled'
import { css } from '@emotion/core'
import Layout from '../components/layout'

export default () => {
  return (
    <Layout>
      <h1>Hello World</h1>
    </Layout>
  )
}
