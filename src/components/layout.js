/* eslint-disable no-unused-vars */
import React from 'react'
import logo from '../../static/logo.svg'
import styled from '@emotion/styled'
import { css, Global } from '@emotion/core'

const Wrapper = styled('section')`
  display: flex;
  max-width: 1440px;
  margin: 0;
  padding: 0;
  font-family: 'Roboto', sans-serif;
  color: #666;
  box-sizing: border-box;
  text-decoration: none;
`
export default ({ children }) => {
  return (
    <Wrapper>
      <Global styles={
        css`
          body, html {
            width: 100%;
            height: 100%;
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            text-decoration: none;
            display: flex;
            flex-direction: column;
            align-items: center;
          }
        `
      }/>
      { children }
    </Wrapper>
  )
}
